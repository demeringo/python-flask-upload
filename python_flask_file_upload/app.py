from flask import Flask

#UPLOAD_FOLDER = 'D:/uploads'
UPLOAD_FOLDER = './tmp'

app = Flask(__name__)
app.secret_key = "my_secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
