# python-flask-upload

A very basic web server to support file upload.
It uses python 3 and flask + bootstrap [https://getbootstrap.com/](https://getbootstrap.com/) for presentation

Application code copied from Soumitra Roy Sarkar work (many thanks !)
- https://www.roytuts.com/python-flask-file-upload-example/
- https://github.com/roytuts/python/tree/master/python_flask_file_upload

And production web server setup (gunicorn + service) comes from: 
- https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-centos-7

## Test usage (dev)

### Setup python virtual env and deps

``` sh
# Create a venv for python 3
python -m venv my_venv
# Switch to this venv (do nto forget .)
. my_venv/Scripts/activate
# Install requirements
pip install -r python_flask_file_upload/requirements.txt

# Exit venv (optional)
deactivate
```

### Start Flask server

``` sh
# use our venv
. my_venv/Scripts/activate
# start flask (from the root of the repository))
python python_flask_file_upload/main.py
```

Go to http://localhost:5000

## Details

- Extension of the file is checked before upload: allowed extensions are ['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
- Files are saved directly to python_flask_file_upload/tmp directory.

See `app.py`.

## Production like setup

This part is written for RHEL 7 / CentOs 7

Use gunicorn + apache or nginx to server application.

```bash
# start gunicorn
cd python_flask_file_upload
gunicorn --bind 0.0.0.0:8000 wsgi
```

Warning: gunicorn may not be fully supported on Windows. My quick test show: `ModuleNotFoundError: No module named 'fcntl'`.

### Install the service

```bash
sudo cp flask_upload.service /etc/systemd/system/flask_upload.service
sudo systemctl start flask_upload
# optional: activate on reboot
#sudo systemctl enable flask_upload
```
